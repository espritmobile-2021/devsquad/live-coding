import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http"

@Injectable({
  providedIn: 'root'
})
export class FilesService {

  url = "http://localhost:4000/"

  constructor(private http: HttpClient) { }


  postFile(file) {
    return this.http.post(this.url + "upload", file)
  }

  getAllFIles() {
    return this.http.get<Array<Object>>(this.url + "files");
  }

  getImageById(image) {
    return this.http.get(this.url + "image/" + image, { responseType: 'arraybuffer' });
  }

  deleteFile(filename){
    return this.http.get(this.url+"files/remove/"+filename);
  }
}
