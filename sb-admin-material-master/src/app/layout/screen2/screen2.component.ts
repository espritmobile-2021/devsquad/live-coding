import { Component, OnInit } from '@angular/core';
import { FilesService } from "../../files.service"


@Component({
  selector: 'app-screen2',
  templateUrl: './screen2.component.html',
  styleUrls: ['./screen2.component.scss']
})
export class Screen2Component implements OnInit {

  file = null
  videos = []
  is_admin

  constructor(private fileService: FilesService) {
    this.is_admin = JSON.parse(localStorage.getItem("user")).is_admin
   }

  ngOnInit() {
    this.getAllVideos()
  }

  getAllVideos() {
    this.fileService.getAllFIles().subscribe(data => {
      this.videos = Array.from(data)
      this.videos = this.videos.filter(el => { return el.contentType.startsWith("video") })
      console.log(this.videos)
    })
  }

  getFile(event) {
    this.file = event.srcElement.files[0]

  }

  sendFile() {
    let formData = new FormData()
    if (this.file) {
      formData.append("file", this.file, this.file.name)

      this.fileService.postFile(formData).subscribe(data => {
        console.log(data)
        this.getAllVideos()
      })
    }

  }

}
