import { Component, OnInit, Input } from '@angular/core';
import { FilesService } from "../../files.service"
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})
export class VideoComponent implements OnInit {

  is_admin
  @Input() video;
  @Input() all;
  src

  constructor(private fileService: FilesService, private sanitizer: DomSanitizer, private router: Router) { }



  ngOnInit(): void {
    this.is_admin = JSON.parse(localStorage.getItem("user")).is_admin  
  }

  onDelete(filename){
    this.fileService.deleteFile(filename).subscribe(data=>{
      this.all.splice( this.all.indexOf(this.video), 1  )

    });
  }

  ngOnChanges() {
    this.fileService.getImageById(this.video.filename).subscribe(data => {
      let blob = new Blob([data], { type: "video/mp4" });
      let url = window.URL.createObjectURL(blob);

      this.src = this.sanitizer.bypassSecurityTrustResourceUrl(url)
    })
  }


}
