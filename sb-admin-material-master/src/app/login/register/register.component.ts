import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {LoginService} from '../../login.service'



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  mail ;
  password;
  name;
  last_name; 
  error ;

  constructor(private router: Router, private loginService: LoginService) { }

  ngOnInit(): void {
  }

  register(){
    
    if (this.mail.search("@gmail")!=-1){
      let param = {
        "username": this.mail,
        "hash": this.password,
        "firstName": this.name,
        "lastName": this.last_name
    }
  
    
    this.loginService.register(param).subscribe((data)=>{
      localStorage.setItem('isLoggedin', 'true');
      this.router.navigate(['/screen2']);
      
  },
  (error)=>{
      this.error = "username exists"
  }
  )
    
    }
    else {
      this.error = "username must be a gmail address"
    }
    

  }

}
