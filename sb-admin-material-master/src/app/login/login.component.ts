import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {LoginService} from '../login.service'

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    mail ;
    password ;
    error = null ;
    constructor(private router: Router, private loginService: LoginService) { }

    ngOnInit() { 
        
    }

    goToRegister(){
        this.router.navigate(['/login/register'])
    }

    onLogin() {

        let formdata = {
            "username":this.mail,
            "password":this.password
        }

        this.loginService.login(formdata).subscribe((data)=>{
            localStorage.setItem('isLoggedin', 'true');
            localStorage.setItem("user", JSON.stringify(data))
            this.router.navigate(['/screen2']);
            
        },
        (error)=>{
            this.error = "verifier mail et mot de passe"
        }
        )

    }


}
