import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http"

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  url = "http://localhost:4000/users/"

  constructor(private http: HttpClient) { }

  login(param){
    console.log(param)
    return this.http.post(this.url+"authenticate", param);
  }


  register(param){
    
    return this.http.post(this.url+"register", param);
  }
  

}
